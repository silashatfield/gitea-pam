apk add g++ musl-dev git make go vim mariadb-dev linux-pam linux-pam-dev
cd /
tar -xf pamtester-0.1.2.tar.gz
cd pamtester-0.1.2
./configure
make install
cp /etc/pam.d/mysql-auth /etc/pam.d/smtp

touch /etc/pam_debug
touch /var/log/auth.log
touch /etc/syslog.conf
echo "*.debug /var/log/auth.log" >> /etc/syslog.conf
syslogd

echo "Use: 'pamtester smtp [wizardname] authenticate' : IE, pamtester smtp krule authenticate"
echo "Check: more /var/log/auth.log"